using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp17
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note_1 = new Note();
            Note note_2 = new Note("RPPOON");
            Note note_3 = new Note("RPPOON", 5);

            Console.WriteLine("Default Constructor: (note_1)");
            Console.WriteLine(note_1.Text);
            Console.WriteLine(note_1.Autor);
            Console.WriteLine(note_1.NumberOfImportance);

            Console.WriteLine("First Constructor: (note_2)");
            Console.WriteLine(note_2.Text);
            Console.WriteLine(note_2.Autor);
            Console.WriteLine(note_2.NumberOfImportance);

            Console.WriteLine("Second Constructor: (note_3)");
            Console.WriteLine(note_3.Text);
            Console.WriteLine(note_3.Autor);
            Console.WriteLine(note_3.NumberOfImportance);

            NoteWithTime TimeNote_1 = new NoteWithTime();
            NoteWithTime TimeNote_2 = new NoteWithTime("RPPOON");
            NoteWithTime TimeNote_3 = new NoteWithTime("RPPOON","Karlo Adzic", 5);

            Console.WriteLine(TimeNote_1.ToString());
            Console.WriteLine(TimeNote_2.ToString());
            Console.WriteLine(TimeNote_3.ToString());

            List<string> notes = new List<string>();

            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("Unesi Autora: ");
                notes.Add(Console.ReadLine());
            }

            List<int> NumbersOfImportanceForNotes = new List<int>();
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("Unesi koliko je biljeska bitna (1-5): ");
                string importanceNumber = Console.ReadLine();
                NumbersOfImportanceForNotes.Add(Convert.ToInt32(importanceNumber));
            }

            Console.WriteLine("Unesi tekst 1. zabiljeske: ");
            string firstNote = Console.ReadLine();
            NoteWithTime NewNote_1 = new NoteWithTime(firstNote, "Karlo Adzic" , 5);

            Console.WriteLine("Unesi tekst 2. zabiljeske: ");
            string secondNote = Console.ReadLine();
            NoteWithTime NewNote_2 = new NoteWithTime(secondNote, "Karlo Adzic" , 3);

            Console.WriteLine("Unesi tekst 3. zabiljeske: ");
            string thirdNote = Console.ReadLine();
            NoteWithTime NewNote_3 = new NoteWithTime(thirdNote, "Karlo Adzic" , 1);

            ToDoList DoingList = new ToDoList();
            DoingList.AddNote(NewNote_1);
            DoingList.AddNote(NewNote_2);
            DoingList.AddNote(NewNote_3);

            for (int i = 0; i < 3; i++)
            {
                if (DoingList.getTheNote(i).NumberOfImportance == 5)
                {
                    DoingList.EraseNote(DoingList.getTheNote(i));
                }
            }

            DoingList.printNote();
        }
    }
}

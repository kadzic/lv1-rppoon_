using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp17
{
    class NoteWithTime : Note
    {
        private DateTime Time { get; set; }
        public NoteWithTime() : base()
        {
            this.Time = DateTime.UtcNow;
        }
        public NoteWithTime(string text) : base(text)
        {
            this.Time = DateTime.UtcNow;
        }
        public NoteWithTime(string text, string autor, int NewNumberOfImportance) : base(text, NewNumberOfImportance)
        {
            this.Time = DateTime.UtcNow;
        }
        public override string ToString()
        {
            return Time.ToString() + ", " + Autor.ToString() + ", " + Text.ToString() + ", " + NumberOfImportance.ToString();
        }
    }
}

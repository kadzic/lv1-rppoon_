using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp17
{
    class Note
    {
        public string Text { get; set; }
        public string Autor { get; private set; }
        public int NumberOfImportance { get; set; } //1 - lowest, 5 - highest

        public Note()
        {
            this.Text = "RPPOON";
            this.Autor = "Karlo Adzic";
            this.NumberOfImportance = 5;
        }
        public Note(string text)
        {
            this.Text = text;
            this.Autor = "Karlo Adzic";
            this.NumberOfImportance = 5;
        }
        public Note(string text, string autor, int NewNumberOfImportance)
        {
            this.Text = text;
            this.Autor = autor;
            this.NumberOfImportance = NewNumberOfImportance;
        }
        public Note(string text, int NewNumberOfImportance)
        {
            this.Text = text;
            this.Autor = "Karlo Adzic";
            this.NumberOfImportance = NewNumberOfImportance;
        }
        public override string ToString()
        {
            return Text.ToString() + ", " + Autor.ToString() + ", " + NumberOfImportance.ToString();
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp17
{
    class ToDoList
    {
        private List<NoteWithTime> notes
        {
            get { return this.notes; }
            set { this.notes = value; }
        }
        public ToDoList()
        {
            this.notes = new List<NoteWithTime>();
        }
        public ToDoList(List<NoteWithTime> notes)
        {
            this.notes = new List<NoteWithTime>(notes);
        }
        public NoteWithTime getTheNote(int index)
        {
            NoteWithTime note = this.notes[index];
            return note;
        }
        public void AddNote(NoteWithTime NewNote)
        {
            notes.Add(NewNote);
        }
        public void EraseNote(NoteWithTime OldNote)
        {
            notes.Remove(OldNote);
        }
        public void printNote()
        {
            foreach (NoteWithTime note in notes)
            {
                Console.Write(note.ToString());
            }
        }
    }
}
